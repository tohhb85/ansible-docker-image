FROM centos
MAINTAINER Toh Haw Beng (tohhb85@gmail.com)
RUN yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
RUN yum install -y epel-release
RUN yum install -y ansible python38 git-core
RUN easy_install-3.8 pip
RUN pip install --upgrade pip setuptools
RUN pip install ansible
RUN pip install pypsexec
RUN pip install pywinrm
RUN pip install pyvmomi
RUN pip install --upgrade git+https://github.com/vmware/vsphere-automation-sdk-python.git
RUN pip install requests --upgrade
RUN ansible-galaxy collection install community.vmware 
RUN ansible-galaxy collection install community.windows
RUN pip install -r ~/.ansible/collections/ansible_collections/community/vmware/requirements.txt
RUN yum install -y traceroute
CMD ["/bin/bash"]
